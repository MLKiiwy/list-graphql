import { DatetimeResolver } from "./Datetime";

// @ todo: define resolver type mapping in another file ?
const resolvers = {
  Datetime: DatetimeResolver
  //  JSON: GraphQLJSON,
};

export default resolvers;
