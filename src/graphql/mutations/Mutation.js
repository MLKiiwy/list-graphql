import ListItem from "../types/ListItem";

const Mutation = `
  type Mutation {
    addUrl(url: String!): ListItem
  }
`;

export default () => [Mutation, ListItem];
