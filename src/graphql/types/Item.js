import ListItem from "./List";
import User from "./User";
import Datetime from "../scalar/Datetime";
import JSON from "../scalar/JSON";

const Item = `
  type Item {
    id: ID!,
    loaded: Boolean,
    type: String!,
    name: String,
    image: String,
    description: String,
    creator: User!,
    createdAt: Datetime!,
    updatedAt: Datetime!,
    data: JSON,
    listItems: [ListItem!]!
  }
`;

export default () => [Item, ListItem, User, Datetime, JSON];
