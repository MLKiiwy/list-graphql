import List from "./List";
import Item from "./Item";
import User from "./User";
import Datetime from "../scalar/Datetime";

const ListItem = `
  type ListItem {
    id: ID!,
    list: List!,
    item: Item!,
    position: Int!,
    name: String,
    image: String,
    description: String,
    creator: User!,
    lastModifier: User!,
    createdAt: Datetime!,
    updatedAt: Datetime!
  }
`;

export default () => [ListItem, List, Item, User, Datetime];
