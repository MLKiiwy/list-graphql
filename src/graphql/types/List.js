import ListItem from "./ListItem";
import User from "./User";
import Datetime from "../scalar/Datetime";
import ListOrder from "../enum/ListOrder";

const List = `
  type List {
    id: ID!
    name: String,
    image: String,
    description: String,
    numberOfItems: Int!,
    itemListOrder: ListOrder!,
    creator: User!,
    lastModifier: User!,
    createdAt: Datetime!,
    updatedAt: Datetime!,
    items: [ListItem!]
  }
`;

export default () => [List, ListItem, User, Datetime, ListOrder];
