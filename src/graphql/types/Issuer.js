const Issuer = `
  type Issuer {
    id: ID!,
    url: String
  }
`;

export default () => [Issuer];
