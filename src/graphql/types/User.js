import List from "./List";
import Issuer from "./Issuer";

const User = `
  type User {
    id: ID!,
    localId: String!,
    displayName: String,
    imageUrl: String,
    emails: [String!]!
    active: List
    lists: [List!],
    issuer: Issuer!
  }
`;

export default () => [User, List, Issuer];
