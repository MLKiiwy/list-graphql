import User from "./types/User";
import Query from "./queries/Query";
import Mutation from "./mutations/Mutation";

export default [Query, Mutation, User];
