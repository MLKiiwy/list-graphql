import List from "../types/List";
import User from "../types/User";

const Query = `
  type Query {
    list(id: String): List,
    lists: [List],
    me: User
  }
`;

export default () => [Query, List, User];
