const ListOrder = `
  enum ListOrder {
    ItemListUnordered,
    ItemListOrdered
  }
`;

export default () => [ListOrder];
