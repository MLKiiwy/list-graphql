import typeDefs from "./graphql";
import resolvers from "./graphql/scalar";

export { resolvers, typeDefs };
