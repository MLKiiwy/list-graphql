module.exports = {
  parser: 'babel-eslint',
  extends: ['airbnb', 'prettier'],
  env: {
    es6: true,
    browser: true,
    jest: true
  },
  rules: {
    'valid-jsdoc': 'error',
    'arrow-parens': ['error', 'as-needed'],
  },
  plugins: ['flowtype'],
};
